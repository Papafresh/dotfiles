#!/bin/sh
xrandr --output DisplayPort-0 --off --output DisplayPort-1 --off --output DisplayPort-2 --mode 1920x1080 --rate 144 --primary --set TearFree on --pos 0x360 --rotate normal --output HDMI-A-0 --mode 2560x1440 --rate 75 --set TearFree on --pos 1920x0 --rotate normal

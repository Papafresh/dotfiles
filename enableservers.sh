#!/bin/bash
REL="$(rpm -E %fedora)"
echo "We are running Fedora $REL."

function setup_repos() {
    # Neomutt
    sudo dnf copr enable flatcap/neomutt
    # I made a typo!
    sudo dnf copr enable ankursinha/rubygem-taskjuggler
    # universal ctags
    dnf copr enable jgoguen/universal-ctags

    # RPMFusion and adobe
    sudo dnf install \
        https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-"$REL".noarch.rpm \
        https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-"$REL".noarch.rpm \
        http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm

    sudo dnf update --refresh -y
}

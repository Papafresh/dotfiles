# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term alacritty
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu rofi -modi drun -show drun \
                -config ~/.config/rofi/rofidmenu.rasi
#font pango:Noto Sans Regular 10
font pango:Jetbrainsmono 10
# exit-menu
bindsym $mod+Shift+e exec rofi -show power-menu -modi power-menu:/home/mariolo/.config/i3/scripts/rofi-power-menu \
-config ~/.config/rofi/rofidmenu.rasi
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output DP-3 bg ~/Pictures/12.jpg fill 
output HDMI-A-1 bg ~/Pictures/23.jpg fill

### Output configuration
output DP-3 resolution 1920x1080@144Hz position 0,0
output HDMI-A-1 resolution 2560x1440@75Hz position 1920,0

focus output DP-3

#Workstyle
#exec_always --no-startup-id workstyle
# Desktop notifications
exec --no-startup-id dbus-launch dunst --config ~/.config/dunst/dunstrc
#get auth work with polkit-xfce
exec --no-startup-id /usr/libexec/xfce-polkit
#protonmail bridge
exec --no-startup-id protonmail-bridge --no-window
#corectrl
exec --no-startup-id corectrl
set $gnome-schema org.gnome.desktop.interface
#Auto Tiling
exec --no-startup-id autotiling
# Sway Fader
#exec python3 /usr/bin/swayfader.py 
#gtk theming
exec_always {
    gsettings set $gnome-schema gtk-theme 'Dracula'
    gsettings set $gnome-schema icon-theme 'Sweet-Rainbow'
    gsettings set $gnome-schema cursor-theme 'Sweet'
    gsettings set $gnome-schema font-name 'Jetbrainsmono'
}
#Audio
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym --locked XF86AudioMicMute exec pactl set-source-mute @DEFAULT_SOURCE@ toggle
#Title bars
default_border pixel 1

### Key bindings
## App shortcuts
bindsym $mod+w exec /usr/bin/firefox 
bindsym $mod+Shift+Return exec thunar 
bindsym $mod+Shift+Print exec xfce4-screenshooter
# start steam
bindsym $mod+Shift+s exec --no-startup-id steam -noverifyfiles
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+q kill

    # Start your launcher
    bindsym $mod+d exec $menu

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+c reload

#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right

#Gaps
gaps inner 10
gaps outer 5

# Workspace names
# to display names or symbols instead of plain workspace $wss you can use
# something like: set $ws1 1:mail
#                 set $ws2 2:
set $ws1 "" 
set $ws2 ""
set $ws3 ""
set $ws4 ""
set $ws5 "" 
set $ws6 ""
set $ws7 ""
set $ws8 ""
set $ws9 ""
set $ws10 ""

#Define outputs
set $output-primary DP-3
set $output-secondary HDMI-A-1

# Workspaces:
workspace $ws1 output DP-3 
workspace $ws2 output DP-3 
workspace $ws3 output DP-3 
workspace $ws4 output DP-3 
workspace $ws5 output DP-3 
workspace $ws6 output HDMI-A-1 
workspace $ws7 output HDMI-A-1 
workspace $ws8 output HDMI-A-1 
workspace $ws9 output HDMI-A-1 
workspace $ws10 output HDMI-A-1 
    
    # Switch to workspace
    bindsym $mod+1 workspace $ws1
    bindsym $mod+2 workspace $ws2
    bindsym $mod+3 workspace $ws3
    bindsym $mod+4 workspace $ws4
    bindsym $mod+5 workspace $ws5
    bindsym $mod+6 workspace $ws6
    bindsym $mod+7 workspace $ws7
    bindsym $mod+8 workspace $ws8
    bindsym $mod+9 workspace $ws9
    bindsym $mod+0 workspace $ws10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace $ws1
    bindsym $mod+Shift+2 move container to workspace $ws2
    bindsym $mod+Shift+3 move container to workspace $ws3
    bindsym $mod+Shift+4 move container to workspace $ws4
    bindsym $mod+Shift+5 move container to workspace $ws5
    bindsym $mod+Shift+6 move container to workspace $ws6
    bindsym $mod+Shift+7 move container to workspace $ws7
    bindsym $mod+Shift+8 move container to workspace $ws8
    bindsym $mod+Shift+9 move container to workspace $ws9
    bindsym $mod+Shift+0 move container to workspace $ws10

# set floating (nontiling)for apps needing it
for_window [app_id="Yad" instance="yad"] floating enable
for_window [app_id="Galculator" instance="galculator"] floating enable
for_window [app_id="Blueberry.py" instance="blueberry.py"] floating enable

for_window [app_id="^Steam$"] floating disable
for_window [app_id="^Steam$" title=" - Steam$"] floating enable
for_window [app_id="^Steam$" title="^About Steam$"] floating enable
for_window [app_id="^Steam$" title="^Add a Game$"] floating enable
for_window [app_id="^Steam$" title="^Create or select new Steam library folder:$"] floating enable
for_window [app_id="^Steam$" title="^Friends List$"] floating enable
for_window [app_id="^Steam$" title="^Install - "] floating enable
for_window [app_id="^Steam$" title="^Product Activation$"] floating enable
for_window [app_id="^Steam$" title="^Properties - "] floating enable
for_window [app_id="^Steam$" title="^Settings$"] floating enable
for_window [app_id="^Steam$" title="^Steam - Error$"] floating enable
for_window [app_id="^Steam$" title="^Steam - News "] floating enable
for_window [app_id="^Steam$" title="^Steam Library Folders$"] floating enable
for_window [app_id="^Steam$" title="^Validating Steam files - "] floating enable

# set floating (nontiling) for special apps
for_window [app_id="Xsane" instance="xsane"] floating enable
for_window [app_id="pavucontrol"] floating enable, resize set width 40 ppt height 30 ppt
for_window [class="qt5ct" instance="qt5ct"] floating enable, resize set width 60 ppt height 50 ppt
for_window [app_id="Blueberry.py" instance="blueberry.py"] floating enable
for_window [app_id="Bluetooth-sendto" instance="bluetooth-sendto"] floating enable
for_window [app_id="Pamac-manager"] floating enable
for_window [app_id="Alacritty"] floating enable, resize set width 60 ppt height 50 ppt 
for_window [app_id=thunar] focus 
for_window [class="^Steam$"] focus
for_window [app_id=firefox] focus
for_window [app_id="xfce-polkit"] floating enable, resize set width 40 ppt height 30 ppt
for_window [app_id="xfce-polkit"] focus
for_window [app_id="corectrl"] floating enable, resize set width 60 ppt height 50 ppt
for_window [app_id="net.davidotek.pupgui2"] floating enable, resize set width 40 ppt height 30 ppt
for_window [app_id=thunar] floating enable, resize set width 60 ppt height 50 ppt
# bind program to workspace and focus to them on startup:
assign [app_id=$term] $ws1
assign [app_id=firefox] $ws2
assign [app_id=thunar] $ws5  
assign [class="^Steam$"] $ws3
assign [app_id="gw2-64.exe"] $ws3
assign [app_id="mpv"] $ws6
assign [app_id="discord"] $ws6
assign [class="thunderbird"] $ws7
assign [app_id="Lutris"] $ws4
assign [app_id="Alacritty"] $ws1
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+Shift+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

######################################
# color settings for bar and windows #
######################################

# Define colors variables:
set $darkbluetrans      #08052be6
set $darkblue           #08052b
set $lightblue          #5294e2
set $urgentred          #e53935
set $white              #ffffff
set $black              #000000
set $purple             #e345ff
set $darkgrey           #383c4a
set $grey               #b0b5bd
set $mediumgrey         #8b8b8b
set $yellowbrown        #e1b700

# define colors for windows:
#app_id                          border          bground         text            indicator       child_border
client.focused              $lightblue  $darkblue       $white          $purple         $mediumgrey
client.unfocused            $darkblue   $darkblue       $grey           $purple         $darkgrey
client.focused_inactive $darkblue       $darkblue       $grey           $purple         $black
client.urgent               $urgentred  $urgentred      $white          $purple         $yellowbrown

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    position top
    swaybar_command waybar
}

include /etc/sway/config.d/*
